#define _CRT_SECURE_NO_WARNINGS
#include "InvertedIndex.h"

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <thread>

#include <windows.h>


std::vector<std::string> getWords(const std::string *path) // Function which split lines into vector of words
{
    std::ifstream file;
    file.open(*path, std::ios::in);// Enter the file

    std::vector<std::string> wordVector;
    std::string line;
    while(std::getline(file, line))// Take line
    {
        std::size_t prev = 0, pos;
        while ((pos = line.find_first_of(" \";,.-?!()<>\\/&*", prev)) != std::string::npos)// Find next delimeter
        {
            if (pos > prev)
                wordVector.push_back(line.substr(prev, pos-prev));
            prev = pos+1;
        }
        if (prev < line.length())
            wordVector.push_back(line.substr(prev, std::string::npos));
    }
    return wordVector;
}

InvertedIndex::InvertedIndex(std::string f, int t) { //Just constructor which makes builds index
    threads = t;
    folder = f;
    BSBIndexing();
    mergeBlocks();
    printIndex();
}

void InvertedIndex::parseFile(std::map<std::string, std::vector<WordCell>> *blockIndex,
        std::string *filename) // Add words from single file to index
{
    const std::string path = folder + "\\" + *filename;
    int wordNum = 1;
    std::vector<std::string> wordsVector = getWords(&path);
    for(const auto &word : wordsVector)
    {
        WordCell cell;
        cell.filename = *filename;
        cell.wordNumber = wordNum;
        (*blockIndex)[word].push_back(cell);
        ++wordNum;
    }
}

void InvertedIndex::parseNextBlock(int threadNum) // Manager for file parsing, multithread
{
    auto *blockIndex = new std::map<std::string, std::vector<WordCell>>;
    for (int i = threadNum; i < filenamesList.size(); i += threads)
        parseFile(blockIndex, &(filenamesList[i]));
    dictionaries[threadNum] = *blockIndex;
    delete blockIndex;
}

std::vector<std::string> InvertedIndex::getFilenames() // Windows-only realization
{
    std::vector<std::string> filenamesList;
    std::string filename;
    WIN32_FIND_DATA data;
    HANDLE hFind = FindFirstFile((folder + "\\*").c_str(), &data);//See files in folder
    if ( hFind != INVALID_HANDLE_VALUE ) {
        do {
            filename = data.cFileName;
            filenamesList.push_back(filename);
        } while (FindNextFile(hFind, &data));
        FindClose(hFind);
    }
    return filenamesList;
}

void InvertedIndex::BSBIndexing()//Manager of block-parsing threads
{
    filenamesList = getFilenames();
    auto worker = new std::thread[threads];
    std::map<std::string, std::vector<WordCell>> emptyBlockIndex;
    for(int i = 0; i < threads; ++i )
    {
        dictionaries.push_back(emptyBlockIndex);
        worker[i] = std::thread(&InvertedIndex::parseNextBlock, this, i);
    }
    for(int i = 0; i < threads; ++i )
        worker[i].join();
}

void InvertedIndex::mergeBlocks()//Just adds items to first index from other indexes
{
    while (dictionaries.size() > 1)
    {
        std::map<std::string, std::vector<WordCell>> blockIndex = dictionaries.back();
        dictionaries.pop_back();
        for(auto &cell : blockIndex)
            for (auto &elem : cell.second)
                dictionaries[0][cell.first].push_back(elem);
    }
}

void InvertedIndex::printIndex() // Use only when you have one merged index
{
    if (dictionaries.size() > 1){
        std::cout << "ERROR: Indexes don`t merged!" << std::endl;
        return;
    }

    for(auto &cell : dictionaries[0])
    {
        std::cout << cell.first << " ";
        for (auto &elem : cell.second)
            std::cout << "(" << elem.filename << "," << elem.wordNumber << ") ";
        std::cout << std::endl;
    }
}


