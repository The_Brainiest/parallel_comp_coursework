#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <windows.h>
#include "InvertedIndex.h"


int main(int argc, TCHAR *argv[])
{
    int threads = 1;
    if (argc != 3 || !(threads = atoi(argv[2])))
    {
        std::cout << "\nUsage: " <<  argv[0] << " <directory name> <threads number>\n";
        return (-1);
    }

    std::string folder = argv[1];
    InvertedIndex(folder, threads);

	return 0;
}