#pragma once
#include <string>
#include <map>
#include <vector>

struct WordCell {
	std::string filename;
	int wordNumber;
};


class InvertedIndex {
private:
	std::vector< std::map<std::string, std::vector<WordCell>> > dictionaries;
	std::vector<std::string> indexFiles;
    int threads;
    std::string folder;
    std::vector<std::string> filenamesList;

public:
    InvertedIndex(std::string f, int t);
    void parseFile(std::map<std::string, std::vector<WordCell>> *blockIndex, std::string *filename);
	void parseNextBlock(int threadNum);
    std::vector<std::string> getFilenames();
	void BSBIndexing();
	void mergeBlocks();
	void printIndex();
};