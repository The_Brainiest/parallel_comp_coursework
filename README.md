# PARALLEL COMPUTATIONS COURSEWORDK - INDEXATOR #

There is a simple comandline tool to build inverted index in folder.

Based on BSBI algorythm - https://nlp.stanford.edu/IR-book/html/htmledition/blocked-sort-based-indexing-1.html

Made by Roman Tsyolkovskyi in may 2020.

### How to build it? ###

You can build this program in Microsoft Visual Studio (2017 and higher versions were tested) 
or with [Visual Studio C++ compiler](https://visualstudio.microsoft.com/vs/features/cplusplus/).

Indexator now a Windows-only tool. All OS-dependable code is stored in 'getFilenames' method realization. 
If you want to make it works for UNIX-compatible OS, it will be great!

### How to use it? ###

Commandline command: parallel_comp_coursework.exe <folder> <threads>

Example: 
```
	parallel_comp_coursework.exe E:\\sample\\train\\unsup 4
```
